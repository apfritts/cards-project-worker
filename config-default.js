
module.exports = {
  database: {
    dsn: null,
  },
  facebook: {
    appId:          process.env.FACEBOOK_APPID,
    appNamespace:   process.env.FACEBOOK_APPNAMESPACE,
    appSecret:      process.env.FACEBOOK_APPSECRET,
    redirectUri:    process.env.FACEBOOK_REDIRECTURI,
  },
  kickq: {
    // See kickq documentation: https://github.com/verbling/kickq/wiki/Configure-Kickq
    redisNamespace: 'cards',
  },
  log4js: {
    // See log4js documentation: https://github.com/nomiddlename/log4js-node/wiki/Appenders
    appenders: [
      { type: 'console' },
      { type: 'file', filename: 'log/cards-worker.log' },
      { type: 'console', category: '*' },
      { type: 'file', filename: 'log/plugins.log', category: '*' },
    ]
  },
};
