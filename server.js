var config = require('./config');
var kickq = require('kickq');
var log = require('./utility/log');
var PluginLoader = require('./utility/plugin_loader');
var Source = require('./models/source');
var SourceType = require('./models/source_type');

kickq.config(config.kickq);
var logger = log.getLogger();

logger.info('Listening for "create"');
kickq.process('create', function(jobItem, data, cb) {
  logger.info('Creating Card Source for ');
  // When a new Card Source is created, this initial job pulls down the entire
  // history of the Source. It also registers any additional hooks with the 
  // Source service.
  PluginLoader.fromSourceId(data.plugin, function(err, plugin, source) {
    if (err) {
      logger.error(err);
      cb(err);
      return;
    }
    plugin.create(function(err) {
      if (err) {
        logger.error(err);
        cb(err);
        return;
      }
      source.last_update = Date.now();
      source.save(function(err) {
        if (err) {
          logger.error(err);
          cb(err);
        } else {
          logger.info('Saved source');
          cb(); 
        }
      });
    });
  });
});

logger.info('Listening for "update"');
kickq.process('update', function(jobItem, data, cb) {
  logger.info('Updating Card Source for ');
  // This is performed periodically and, if supported, when a source calls
  // the associated web hook. Web hook data is pased in the data['webhook']
  // parameter.
});

logger.info('Listening for "sync"');
kickq.process('sync', function(jobItem, data, cb) {
  logger.info('Syncing Card Source for ');
  // This is performed when the user click "Force Sync" in the administration
  // panel. This is used to pull in cards when the user notices some are missing.
});

logger.info('Listening for "delete"');
kickq.process('delete', function(jobItem, data, cb) {
  logger.info('Deleting Card Source for ');
  // When the user deletes a Source, this is called to cleanup the cards and 
  // unregister any webhooks. This function will delete the cards and Source 
  // after the plugin has run its cleanup.
});
