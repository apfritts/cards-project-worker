var Card = require('../models/card');
var cheerio = require('cheerio');
var log = require('../utility/log');
var Q = require('q');
var request = require('request');
var URL = require('url');

function PhpBBPlugin(source) {
  this.source = source;
  this.logger = log.getLogger('phpbb-cheerio');
}

PhpBBPlugin.prototype.create = function(done) {
  this.sync(done);
};

PhpBBPlugin.prototype.update = function(done) {
  this.sync(done);
}

PhpBBPlugin.prototype.sync = function(done) {
  var req = request.defaults({jar: request.jar()});
  var results = {};
  var self = this;

  function processResultPage(dom) {
    var topics = dom('table.forumline tr a.topictitle');
    topics.each(function(i, topic) {
      var url = URL.parse(topic.attribs.href, true);
      delete url.search;
      if (!url.host) {
        url.protocol = 'https';
        url.host = 'forums.gentoo.org';
      }
      delete url.query.sid;
      cardData = {title: topic.children[0].data, url: URL.format(url)};
      results[cardData.url] = cardData;
      self.logger.debug(results[cardData.url]);
    });
  }

  function getNextLink(dom) {
    var result = null;
    dom('a').each(function(i, link) {
      if (link.children && link.children.length == 1 && link.children[0].data == 'Next') {
        result = link.attribs.href;
      }
    });
    return result;
  }

  function getNextResultPage(rawUrl) {
    url = URL.parse(rawUrl, true);
    if (!url.host) {
      url.protocol = 'https';
      url.host = 'forums.gentoo.org';
    }
    var absUrl = URL.format(url);
    self.logger.info('Retrieving ' + absUrl);
    req.get(absUrl, retrieveResultPage);
  }

  function retrieveResultPage(err, response, body) {
    if (err) {
      self.logger.error(err);
      return;
    }
    self.logger.info('Domifying result page');
    var dom = cheerio.load(body);
    processResultPage(dom);
    var nextA = getNextLink(dom);
    if (nextA) {
      getNextResultPage(nextA);
    } else {
      performSync();
    }
  }

  function performSync() {
    Card.find({source_id: self.source._id}).exec(function(err, cards) {
      // Search all results, remove ones that aren't in cards
      self.logger.info('Starting with ' + Object.keys(results).length + ' results');
      var promises = [];

      for (var i = 0; i < cards.length; i++) {
        var card = cards[i];
        // If the URL doesn't exist, remove this
        if (results.hasOwnProperty(card.url)) {
          // Update the current card
          if (card.title !== results[card.url].title) {
            card.title = results[card.url].title;
            promises.push(card.save());
          }
          delete results[card.url];
        } else {
          // Delete this card, we can't find it anymore
          promises.push(card.remove());
        }
      }

      self.logger.info('Creating ' + Object.keys(results).length + ' cards');
      for (var url in results) {
        var cardData = results[url];
        cardData.source_id = self.source._id;
        self.logger.info(cardData);
        var card = new Card(cardData);
        promises.push(card.save());
      }
      if (promises.length) {
        self.logger.info('Saving to mongoose...');
        Q.allSettled(promises).then(function(results) {
          done();
        });
      } else {
        done();
      }
    });
  }

  req.post({url: 'https://forums.gentoo.org/search.php', form: {search_author: 'hellboi64'}}, retrieveResultPage);
}

PhpBBPlugin.prototype.delete = function(done) {
  done();
};

module.exports = PhpBBPlugin;
