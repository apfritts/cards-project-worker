var FB = require('fb');
var config = require('./config');
var log = require('../common/log');

FB.options(config.facebook);

function FacebookPlugin(source) {
  this.source = source;
  this.logger = log.getLogger('facebook');
}

FacebookPlugin.prototype.create = function(done) {
  FB.setAccessToken(this.source.data.access_token);
  FB.api('/me/feed', function(res) {
    if (!res || res.error) {
      this.logger.error(!res ? 'error occurred' : res.error);
    } else {
      this.logger.info(res.data[0]);
      this.logger.info(res.paging);
    }
    done();
  });
};

FacebookPlugin.prototype.update = function(done) {
  done();
}

FacebookPlugin.prototype.sync = function(done) {
  done();
}

FacebookPlugin.prototype.delete = function(done) {
  done();
};

module.exports = FacebookPlugin;
