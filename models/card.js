var mongoose = require('../utility/db');

var schema = new mongoose.Schema({
  content: String,
  source_id: mongoose.Schema.Types.ObjectId,
  url: String,
});
module.exports = mongoose.model('Card', schema);
