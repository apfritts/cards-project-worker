var mongoose = require('../utility/db');

var schema = new mongoose.Schema({
  data: mongoose.Schema.Types.Mixed,
  enabled: Boolean,
  name: String,
  plugin: String,
});
module.exports = mongoose.model('SourceType', schema);
