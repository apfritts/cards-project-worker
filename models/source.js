var mongoose = require('../utility/db');

var schema = new mongoose.Schema({
  data: mongoose.Schema.Types.Mixed,
  enabled: Boolean,
  last_update: Date,
  name: String,
  type: String,
});
module.exports = mongoose.model('Source', schema);
