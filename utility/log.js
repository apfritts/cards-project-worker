var config = require('../config');
var log4js = require('log4js');

log4js.configure(config.log4js);

module.exports = {
  getLogger: function(name) {
    return log4js.getLogger(name);
  },
};
