var config = require('../config');
var log = require('./log');
var Source = require('../models/source');
var SourceType = require('../models/source_type');

var logger = log.getLogger('plugin_loader');

function getPlugin(source, name, cb) {
  logger.info('Loading plugin for source ' + source + ' named ' + name);
  var plugin = null;
  try {
    var Plugin = require('../plugins/' + name);
    plugin = new Plugin(source);
  } catch(e) {
    cb(new Error('Unable to load plugin ./plugins/' + name + ': ' + e.message), null);
    return;
  }
  cb(null, plugin);
}

function getSource(name, cb) {
  logger.debug('getSource for ' + name);
  Source.findOne({enabled: true, name: name}).exec(function(err, source) {
    logger.debug(source);
    if (err) {
      cb(err, null);
    } else if (!source) {
      cb(new Error('No enabled source called ' + name), null);
    } else {
      cb(null, source);
    }
  });
}

function getSourceType(name, cb) {
  logger.debug('getSourceType for ' + name);
  SourceType.findOne({name: name}).exec(function(err, sourceType) {
    logger.debug(sourceType);
    if (err) {
      cb(err, null);
    } else if (!sourceType) {
      cb(new Error('No source type with name ' + name + ' found'), null);
    } else {
      cb(null, sourceType);
    }
  });
}

module.exports = {
  fromSourceId: function(sourceId, cb) {
    getSource(sourceId, function(err, source) {
      if (err) {
        cb(err, null, null);
        return;
      }
      getSourceType(source.type, function(err, sourceType) {
        if (err) {
          cb(err, null, null);
          return;
        }
        getPlugin(source, sourceType.plugin, function(err, plugin) {
          if (err) {
            cb(err, null, null);
          } else {
            cb(null, plugin, source);
          }
        });
      })
    });
  }
};
